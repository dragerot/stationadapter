package net.toregard.stationadapter.resource

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api")
class PingResource(@Value("\${pingmessage.text}") val pingMessage: String = "ok") {

    @GetMapping("/ping")
    fun ping(): ResponseEntity<String> {
        return ResponseEntity.ok(pingMessage)
    }

    @GetMapping("/extraping")
    fun pingExtra(): ResponseEntity<String> {
        return ResponseEntity.ok("Ok eostraping")
    }
}

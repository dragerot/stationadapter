package net.toregard.stationadapter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StationadapterApplication

fun main(args: Array<String>) {
    runApplication<StationadapterApplication>(*args)
}
